import csv
from sklearn.ensemble import RandomForestRegressor
import pickle

file = open(r"C:\dev\CodeJam2013\firsthalf.csv")
reader = csv.reader(file,delimiter=',')
reader.next()
target = [float(x[7]) for x in reader]

file.seek(0)
reader.next()
train = [[float(y) for y in x[0:7]] for x in reader]

#file2 = open(r"C:\dev\CodeJam2013\week2.csv")
#reader2 = csv.reader(file2,delimiter=',')
#reader2.next()
#test = [[float(y) for y in x] for x in reader2]

rf = RandomForestRegressor(n_estimators=10, min_samples_split=2, n_jobs=1)
rf.fit(train, target)


modelFile = open("randomforest-firsthalf.model",'w')
pickle.dump(rf, modelFile)
modelFile.close()

#loadFile = open("randomforest.model",'r')
#model = pickle.load(loadFile)
#loadFile.close()

#predictions = model.predict(test)
#fileout = open('week2_pred.csv', 'w')
#for pred in predictions:
#    fileout.write(str(pred))
#    fileout.write('\n')
#




file.close()