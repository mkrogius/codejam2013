import datetime
import dateutil.parser
import pickle
from sklearn.ensemble import RandomForestRegressor

class Predictor(object):
    def __init__(self):
        modelfile = open('randomforest.model', 'r')
        self.model = pickle.load(modelfile)
        modelfile.close()

    def interpolate(self,prev, next, step):
        return (next - prev)*(step/4.0)+prev
    

    def preprocessData(self,data):
        date = dateutil.parser.parse(data[0])
        utcdate = date - date.utcoffset()
        retdata = []
        doy = (utcdate - datetime.datetime(utcdate.year,1,1,0,0,0,0,utcdate.tzinfo)).days
        retdata.append(float(doy))
        retdata.append(float(utcdate.weekday()))
        retdata.append(float(utcdate.hour*60+utcdate.minute))

        retdata.append(float(data[1]))
        retdata.append(float(data[2]))
        retdata.append(float(data[3]))
        retdata.append(float(data[4]))

        return retdata


    def preprocessRequest(self,req):
        output = []

        #We don't know where the first weather data is
        firstIndex = 0
        for i,line in enumerate(req):
            if line.split(",")[1]:
                #We found the first line with data
                firstIndex = i
                break

        prevdata = req[firstIndex].split(',')

        #For all the lines before the first weather data we don't interpolate
        for i in range(0,firstIndex):
            linedata = req.pop(0).split(',')
            for j in range(1,5):
                linedata[j] = prevdata[j]
            output.append(self.preprocessData(linedata))

        #Process the prevData line
        req.pop(0)
        output.append(self.preprocessData(prevdata))

        lines = ['1','2','3'] #allocation!

        while True:
            lines[0] = req.pop(0)
            lines[1] = req.pop(0)
            lines[2] = req.pop(0)
            try:
                next = req.pop(0)#We know the last line is a multiple of 4
            except IndexError:
                #We do the last lines without interpolation
                for line in lines:
                    linedata = line.split(",")
                    for j in range(1,5):
                        linedata[j] = prevdata[j]
                    output.append(self.preprocessData(linedata))
                break


            nextdata = next.split(",")
            for i,line in enumerate(lines):
                linedata = line.split(",")
                for j in range(1,5):
                    linedata[j] = self.interpolate(float(prevdata[j]),float(nextdata[j]),i+1)
                output.append(self.preprocessData(linedata))
            output.append(self.preprocessData(nextdata))
            prevdata = nextdata

        return output

    def getPrediction(self,data):
        return self.model.predict(data)


#p = Predictor()
#lines = open(r"C:\dev\CodeJam2013\sample_input.csv").readlines()
#lines.pop(0)
#lines.pop(0)
#p.preprocessRequest(lines)