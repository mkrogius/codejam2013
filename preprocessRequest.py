import datetime
import dateutil.parser

def interpolate(prev, next, step):
    return (next - prev)*(step/4.0)+prev
    

def preprocessData(data):
    date = dateutil.parser.parse(data[0])
    utcdate = date.astimezone()
    retdata = []
    doy = (utcdate - datetime.datetime(utcdate.year,1,1,0,0,0,0,utcdate.tzinfo)).days
    retdata.append(float(doy))
    retdata.append(float(utcdate.weekday()))
    retdata.append(float(utcdate.hour*60+utcdate.minute))

    retdata.append(float(data[1]))
    retdata.append(float(data[2]))
    retdata.append(float(data[3]))
    retdata.append(float(data[4]))

    return retdata


def preprocessRequest(req):
    output = []

    prev = req.pop(0)
    prevdata = prev.split(",")
    output.append(preprocessData(prevdata))

    lines = ['1','2','3'] #allocation!

    while True:
        lines[0] = req.pop(0)
        lines[1] = req.pop(0)
        lines[2] = req.pop(0)
        try:
            next = req.pop(0)#We know the last line is a multiple of 4
        except IndexError:
            #We do the last lines without interpolation
            for i,line in enumerate(lines):
                linedata = line.split(",")
                for j in range(1,5):
                    linedata[j] = prevdata[j]
                output.append(preprocessData(linedata))
            break


        nextdata = next.split(",")
        for i,line in enumerate(lines):
            linedata = line.split(",")
            for j in range(1,5):
                linedata[j] = interpolate(float(prevdata[j]),float(nextdata[j]),i+1)
            output.append(preprocessData(linedata))
        output.append(preprocessData(nextdata))
        prevdata = nextdata

    return output